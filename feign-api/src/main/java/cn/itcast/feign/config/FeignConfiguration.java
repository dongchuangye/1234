package cn.itcast.feign.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "cn.itcast.feign.fallback")
@EnableFeignClients(basePackages ="cn.itcast.feign.client",defaultConfiguration = DefaultFeignConfiguration.class)
public class FeignConfiguration {
}
